Feature: Customers

  Background:
    Given I visit the Studio URL
    And The login page is open
    And I log in to the studio company
    And The Studio is open

  Scenario: Create a new customer
    Given I open the Customers page
    And The Customers page is open
    When I click on the New Customer link
    And The New Customer page is open
    And I enter valid details
    And I submit the customer form
    Then The new customer is created
    And All of the customer's details are correct

  Scenario: Edit a customer
    Given I open the Customers page
    And The Customers page is open
    And I have a customer configured
    When I click Edit
    And I edit the customer
    Then The customer is edited
    And The old customer details are not present

  Scenario: Fail to make a new customer with no details
    Given I open the Customers page
    And The Customers page is open
    When I click on the New Customer link
    And The New Customer page is open
    And I submit the customer form
    Then A new customer is not created