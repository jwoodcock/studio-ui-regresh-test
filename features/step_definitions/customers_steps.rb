When("I click on the New Customer link") do
  find('a[ui-sref="clients.new()"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

When("The New Customer page is open") do
  expect(page).to have_css('legend', text: 'Basic Details')
  expect(page).to have_css('legend', text: 'Additional Details')
  expect(page).to have_css('input[name="first_name"]')
  expect(page).to have_css('input[name="last_name"]')
  expect(page).to have_css('input[name="email"]')
  expect(page).to have_css('input[type="tel"]', count: 2)
  expect(page).to have_css('select[name="member_type"]')
  expect(page).to have_css('input[ng-model="datetimeWithNoTz"]')
  expect(page).to have_css('input[name="reference"]')
  expect(page).to have_css('select[name="time_zone"]')
  expect(page).to have_css('input[name="address1"]')
  expect(page).to have_css('input[name="address2"]')
  expect(page).to have_css('input[name="address3"]')
  expect(page).to have_css('input[name="address4"]')
  expect(page).to have_css('input[name="address5"]')
  expect(page).to have_css('input[name="postcode"]')
  expect(page).to have_css('select[name="country"]')
  expect(page).to have_css('input[value="Save"]')
end

When("I enter valid details") do
  $studiouser = new_user
  find('input[name="first_name"]').set $studiouser.firstname
  find('input[name="last_name"]').set $studiouser.surname
  find('input[name="email"]').set $studiouser.email
  page.all('input[type="tel"]')[0].set $studiouser.mobile
  $mobile2 = (7700000000 + rand(99999999)).to_s
  page.all('input[type="tel"]')[1].set $mobile2
  find('select[name="member_type"]').select 'Member'
  find('button[title="Select date"]').click
  expect(page).to have_css('div[class="ng-scope ng-isolate-scope uib-datepicker"]')
  within(find('div[class="ng-scope ng-isolate-scope uib-datepicker"]')) do
    find('td[ng-repeat="dt in row"]', match: :first).click
  end
  $join_date = find('input[ng-model="datetimeWithNoTz"]').value
  $reference = ([*('A'..'Z')].sample(1).join + [*('a'..'z')].sample(7).join)
  find('input[name="reference"]').set $reference
  within('select[name="time_zone"]') do
    page.all('option').count
    $timezone = page.all('option')[1 + rand(page.all('option').count)].text
  end
  find('select[name="time_zone"]').select $timezone
  find('input[name="address1"]').set $studiouser.address
  $address2 = ([*('A'..'Z')].sample(1).join + [*('a'..'z')].sample(7).join)
  find('input[name="address2"]').set $address2
  $address3 = ([*('A'..'Z')].sample(1).join + [*('a'..'z')].sample(7).join)
  find('input[name="address3"]').set $address3
  $town = ([*('A'..'Z')].sample(1).join + [*('a'..'z')].sample(7).join)
  find('input[name="address4"]').set $studiouser.town
  $county = ([*('A'..'Z')].sample(1).join + [*('a'..'z')].sample(7).join)
  find('input[name="address5"]').set $county
  find('input[name="postcode"]').set $studiouser.postcode
  within('select[name="country"]') do
    page.all('option').count
    $country = page.all('option')[rand(page.all('option').count)].text
  end
  find('select[name="country"]').select $country
end

When("I submit the customer form") do
  find('input[value="Save"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("The new customer is created") do
  step 'The Customers page is open'
  page.evaluate_script('window.location.reload()')
  step 'The Customers page is open'
  within('th[field-name="name"]') do
    find('input').set $studiouser.firstname + ' ' + $studiouser.surname
  end
  within('tr', text: $studiouser.mobile) do
    expect(page).to have_css('a', text: $studiouser.firstname + ' ' + $studiouser.surname)
    expect(page).to have_css('a', text: $studiouser.email)
    expect(page).to have_css('div', text: $studiouser.mobile)
  end
end

Then("All of the customer's details are correct") do
  within('tr', text: $studiouser.email) do
    find('a[ui-sref="clients.edit({id: gridItem.id})"]', match: :first).click
  end
  expect(page).to have_content('Upcoming Bookings')
  find('li[id="client-details-tab"]').click
  expect(page).to have_css('legend', text: 'Basic Details')
  expect(page).to have_css('legend', text: 'Additional Details')
  expect(find('input[name="first_name"]').value).to eql($studiouser.firstname)
  expect(find('input[name="last_name"]').value).to eql($studiouser.surname)
  expect(find('input[name="email"]').value).to eql($studiouser.email)
  expect(page.all('input[type="tel"]')[0].value).to eql($studiouser.mobile)
  expect(page.all('input[type="tel"]')[1].value).to eql($mobile2)
  expect(page).to have_select('member_type', selected: 'Member')
  expect(find('input[name="reference"]').value).to eql($reference)
  expect(page).to have_select('time_zone', selected: $timezone)
  expect(find('input[name="address1"]').value).to eql($studiouser.address)
  expect(find('input[name="address2"]').value).to eql($address2)
  expect(find('input[name="address3"]').value).to eql($address3)
  expect(find('input[name="address4"]').value).to eql($studiouser.town)
  expect(find('input[name="address5"]').value).to eql($county)
  expect(find('input[name="postcode"]').value).to eql($studiouser.postcode)
  expect(page).to have_select('country', selected: $country)
end

Then("A new customer is not created") do
  expect(page).to have_css('div[role="alert"]', text: 'Something went wrong')
  step 'The New Customer page is open'
end

Given("I have a customer configured") do
  if defined? $studiouser
    within('th[field-name="name"]') do
      find('input').set $studiouser.firstname + ' ' + $studiouser.surname
    end
    expect(page).to have_css('a', text: $studiouser.firstname + ' ' + $studiouser.surname)
    within('tr', text: $studiouser.mobile) do
      expect(page).to have_css('a', text: $studiouser.firstname + ' ' + $studiouser.surname)
      expect(page).to have_css('a', text: $studiouser.email)
      expect(page).to have_css('div', text: $studiouser.mobile)
    end
  else
    step 'I click on the New Customer link'
    step 'The New Customer page is open'
    step 'I enter valid details'
    step 'I submit the customer form'
    step 'The new customer is created'
  end
  expect(page).to have_css('span[translate="ADMIN_DASHBOARD.CLIENTS_PAGE.EDIT"]', count: 1)
end

When("I click Edit") do
  find('a[ui-sref="clients.edit({id: gridItem.id})"]', match: :first).click
end

When("I edit the customer") do
  expect(page).to have_content('Upcoming Bookings')
  find('li[id="client-details-tab"]').click
  expect(page).to have_css('legend', text: 'Basic Details')
  expect(page).to have_css('legend', text: 'Additional Details')
  $oldstudiouser = $studiouser
  step 'I enter valid details'
  find('input[value="Save"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
  expect(page).to have_content('Basic Details')
end

Then("The customer is edited") do
  expect(find('input[name="first_name"]').value).to eql($studiouser.firstname)
  expect(find('input[name="last_name"]').value).to eql($studiouser.surname)
  expect(find('input[name="email"]').value).to eql($studiouser.email)
  expect(page.all('input[type="tel"]')[0].value).to eql($studiouser.mobile)
  expect(page.all('input[type="tel"]')[1].value).to eql($mobile2)
  expect(page).to have_select('member_type', selected: 'Member')
  expect(find('input[ng-model="datetimeWithNoTz"]').value).to eql($join_date)
  expect(find('input[name="reference"]').value).to eql($reference)
  expect(page).to have_select('time_zone', selected: $timezone)
  expect(find('input[name="address1"]').value).to eql($studiouser.address)
  expect(find('input[name="address2"]').value).to eql($address2)
  expect(find('input[name="address3"]').value).to eql($address3)
  expect(find('input[name="address4"]').value).to eql($studiouser.town)
  expect(find('input[name="address5"]').value).to eql($county)
  expect(find('input[name="postcode"]').value).to eql($studiouser.postcode)
  expect(page).to have_select('country', selected: $country)
  step 'I open the Customers page'
  step 'The new customer is created'
end

Then("The old customer details are not present") do
  expect(page).to have_no_css('a', text: $oldstudiouser.firstname + ' ' + $oldstudiouser.surname)
  expect(page).to have_no_css('a', text: $oldstudiouser.email)
  expect(page).to have_no_css('div', text: $oldstudiouser.mobile)
end
