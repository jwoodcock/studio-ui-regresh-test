Given("I am not on the Staff Calendar") do
  if page.has_css?('div[id="uicalendar"]', text: 'Staff')
    find('a[href="#/calendar/resources/"]').click
    expect(page).to have_css('div[id="uicalendar"]', text: 'Resources')
  end
end

When("I open the Staff Calendar") do
  find('a[href="#/calendar/people/"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("The {string} Calendar is open") do |object|
  expect(page).to have_css('span[class="slider  switch-left"]')
  expect(page).to have_css('div[id="uicalendar"]', text: object)
  expect(page).to have_css('button[class="fc-today-button fc-button fc-state-default fc-corner-left fc-state-disabled"]', text: 'Today')
  expect(page).to have_css('span[class="fc-icon fc-icon-left-single-arrow"]')
  expect(page).to have_css('span[class="fc-icon fc-icon-right-single-arrow"]')
  expect(page).to have_css('p[class="input-group datepicker"]')
  expect(page).to have_css('button', text: 'Calendar')
  expect(page).to have_css('button', text: 'Agenda')
  expect(page).to have_css('button', text: 'Day')
  expect(page).to have_css('button', text: 'Week')
  expect(page).to have_css('button', text: 'Month')
  expect(page).to have_css('h1', text: Time.now.strftime('%B %-d, %Y'))
end

When("I open the Resources Calendar") do
  expect(page).to have_css('a[href="#/calendar/people/"]')
  find('a[href="#/calendar/resources/"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

When("I open the Customers page") do
  find('a[href="#/clients/all"]', match: :first).click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("The Customers page is open") do
  expect(page).to have_css('h1[translate="ADMIN_DASHBOARD.CLIENTS_PAGE.CLIENTS"]')
  expect(page).to have_css('a[href="#/clients/new"]')
  expect(page).to have_css('th[field-name="name"]')
  within(find('th[field-name="name"]')) do
    expect(page).to have_css('input')
  end
  expect(page).to have_css('th[field-name="email"]')
  within(find('th[field-name="email"]')) do
    expect(page).to have_css('input')
  end
  expect(page).to have_css('th[field-name="mobile"]')
  within(find('th[field-name="mobile"]')) do
    expect(page).to have_css('input')
  end
  expect(page).to have_css('input[placeholder="Search"]')
end

When("I open the Check In page") do
  find('a[href="#/check-in"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("The Check In page is open") do
  expect(page).to have_css('h1[translate="ADMIN_DASHBOARD.CHECK_IN_PAGE.CHECK_IN"]')
  expect(page).to have_css('button[translate="ADMIN_DASHBOARD.CHECK_IN_PAGE.WALK_IN"]')
  expect(page).to have_css('th', text: 'Edit')
  expect(page).to have_css('th', text: 'Customer')
  expect(page).to have_css('th', text: 'Staff Member')
  expect(page).to have_css('th', text: 'Due')
  expect(page).to have_css('th', text: 'No Show')
  expect(page).to have_css('th', text: 'Arrived')
  expect(page).to have_css('th', text: 'Being Seen')
  expect(page).to have_css('th', text: 'Completed')
end

When("I open the top right drop down menu") do
  find('a[class="dropdown-toggle"]', text: 'Hi,').click
end

Then("The top right drop down menu is open") do
  expect(page).to have_css('a[ui-sref="logout"]')
  expect(page).to have_css('span[translate="ADMIN_DASHBOARD.CORE.SWITCH_TO_CLASSIC"]')
  within('div[class="bb-user-preferences"]') do
    expect(page).to have_css('span[class="slider  switch-right"]')
  end
  expect(page).to have_css('span[translate="I18N.TIMEZONE.TIMEZONE_LABEL"]')
end

When("I click Logout") do
  find('a[ui-sref="logout"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("I am logged out") do
  step 'The login page is open'
end

When("I click Switch to Classic") do
  find('span[translate="ADMIN_DASHBOARD.CORE.SWITCH_TO_CLASSIC"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("The classic rails app interface is open") do
  expect(page).to have_css('a[href="/company/mycompany"]')
end

Given("I open the language drop down menu") do
  find('li[id="language-picker"]').click
end

Given("The language drop down menu is open") do
  expect(page).to have_css('ul[class="dropdown-menu"]')
  expect(page).to have_css('a', text: 'English')
  expect(page).to have_css('a', text: 'Français')
end

When("I change the language to French") do
  click_link('Français')
end

Then("The date on the calendar is translated to French") do
  french_months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']
  expect(page).to have_css('h2', text: Time.now.strftime('%-d ' + french_months[Time.now.strftime('%-m').to_i - 1] + ' %Y'))
end
