When("I enter a valid url") do
  find_field('site').set 'https://qauk-staging-2.bookingbug.com'
end

When("I enter an invalid string in the url field") do
  find_field('site').set [*('A'..'Z')].sample(1).join + [*('a'..'z')].sample(7).join
end

When("I enter an incorrect url") do
  find_field('site').set 'https://uk.bookingbug.com'
end

When("I enter a valid email address for the bookings company") do
  find_field('username').set 'qa+bookings@bookingbug.com'
end

When("I enter a valid password for the bookings company") do
  find_field('password').set 'bn56tygh'
end

When("I enter a valid email address for the config company") do
  find_field('username').set 'qa+config@bookingbug.com'
end

When("I enter a valid password for the config company") do
  find_field('password').set 'cv34erdf'
end

When("I click Login") do
  click_button('Login')
end

Then("I am successfully logged in") do
  expect(page).to have_content('Calendar')
  expect(page).to have_content('Hi,')
end

Then("I am not logged in") do
  step 'The login page is open'
end

When("I enter an invalid string in the email field") do
  find_field('username').set [*('A'..'Z')].sample(1).join + [*('a'..'z')].sample(7).join
end

When("I enter an incorrect email address") do
  find_field('username').set [*('A'..'Z')].sample(1).join + [*('a'..'z')].sample(7).join + '@example.com'
end

When("I enter an incorrect password") do
  find_field('password').set ([*('A'..'Z'),*('a'..'z'),*('0'..'9')]).sample(rand(40) + 1).join
end