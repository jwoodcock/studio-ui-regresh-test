When("I click on the {string} calendar") do |calendar|
  within('div[class="fc-right"]') do
    click_button(calendar)
  end
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("The Agenda calendar is open") do
  expect(page).to have_css('div[class="fc-view fc-bbListDay-view fc-list-view fc-widget-content"]')
  expect(page).to have_css('h1', text: Time.now.strftime('%B %-d, %Y'))
end

Then("The Week calendar is open") do
  expect(page).to have_css('div[class="fc-view fc-agendaWeek-view fc-agenda-view"]')
  expect(page).to have_css('th', text: 'Sun')
  expect(page).to have_css('th', text: 'Mon')
  expect(page).to have_css('th', text: 'Tue')
  expect(page).to have_css('th', text: 'Wed')
  expect(page).to have_css('th', text: 'Thu')
  expect(page).to have_css('th', text: 'Fri')
  expect(page).to have_css('th', text: 'Sat')
  sunday = Date.today - Date.today.wday
  saturday = Date.today - Date.today.wday + 6
  if sunday.strftime('%b') == saturday.strftime('%b')
    sunday = sunday.strftime('%-d')
    saturday = saturday.strftime('%-d')
  else
    sunday = sunday.strftime('%-d')
    saturday = saturday.strftime('%b %-d')
  end
  expect(page).to have_css('h1', text: Time.now.strftime('%b ') + sunday + ' – ' + saturday + Time.now.strftime(', %Y'))
end

Then("The Month calendar is open") do
  expect(page).to have_css('h1', text: Time.now.strftime('%B %Y'))
  expect(page).to have_css('th', text: 'Sun')
  expect(page).to have_css('th', text: 'Mon')
  expect(page).to have_css('th', text: 'Tue')
  expect(page).to have_css('th', text: 'Wed')
  expect(page).to have_css('th', text: 'Thu')
  expect(page).to have_css('th', text: 'Fri')
  expect(page).to have_css('th', text: 'Sat')
end

When("I click the {string} arrow") do |arrow|
  find('span.fc-icon.fc-icon-' + arrow + '-single-arrow').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("Tomorrow's calendar is open") do
  expect(page).to have_css('h1', text: (DateTime.now + 1).strftime('%B %-d, %Y'))
end

Then("Yesterday's calendar is open") do
  expect(page).to have_css('h1', text: (DateTime.now - 1).strftime('%B %-d, %Y'))
end

When("I open the datepicker") do
  find('button[class="btn btn-default datepicker-popup"]').click
end

When("The datepicker is open") do
  expect(page).to have_css('div[class="uib-daypicker ng-scope"]')
end

When("I pick a random date") do
  datenumber = rand(page.all('button.btn.btn-default.btn-sm').count)
  date = page.all('button.btn.btn-default.btn-sm')[datenumber].text.to_i
  if datenumber < 12 && date > 24
    $date = Date.today.prev_month.strftime('%B ' + date.to_s + ', %Y')
  elsif datenumber > 24 && date < 12
    $date = Date.today.next_month.strftime('%B ' + date.to_s + ', %Y')
  else
    $date = Date.today.strftime('%B ' + date.to_s + ', %Y')
  end
  page.all('button.btn.btn-default.btn-sm')[datenumber].click
end

Then("The correct date's calendar will be open") do
  expect(page).to have_css('h1', text: $date)
end

When("I click Today") do
  find('button[class="fc-today-button fc-button fc-state-default fc-corner-left"]').click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("Today's calendar will be open again") do
  expect(page).to have_css('h1', text: Time.now.strftime('%B %-d, %Y'))
end

When("I click the show toggle") do
  find('span[class="slider  switch-left"]').click
end

When(/^I select a (?:staff member|resource)$/) do
  expect(page).to have_css('input[aria-label="Choose a person"]')
  find('input[aria-label="Choose a person"]').click
  expect(page).to have_css('a[class="ui-select-choices-row-inner"]')
  staff_number = rand(page.all('a[class="ui-select-choices-row-inner"]').count)
  $staff_selection = page.all('a[class="ui-select-choices-row-inner"]')[staff_number].text
  page.all('a[class="ui-select-choices-row-inner"]')[staff_number].click
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Then("I can only see the individual staff member's calendar") do
  within('td[class="fc-resource-area fc-widget-content"]', text: 'Staff') do
    expect(page).to have_css('span[class="fc-cell-text"]', count: 2)
    expect(page).to have_css('span[class="fc-cell-text"]', text: 'Staff', match: :prefer_exact)
    expect(page).to have_css('span[class="fc-cell-text"]', text: $staff_selection, match: :prefer_exact)
  end
end

Then("I can only see the individual resource's calendar") do
  within('td[class="fc-resource-area fc-widget-content"]', text: 'Resource') do
    expect(page).to have_css('span[class="fc-cell-text"]', count: 2)
    expect(page).to have_css('span[class="fc-cell-text"]', text: 'Resource', match: :prefer_exact)
    expect(page).to have_css('span[class="fc-cell-text"]', text: $staff_selection, match: :prefer_exact)
  end
end

When("I click on a date on the month calendar") do
  good = false
  while good == false
    timeslot = rand(page.all('td[data-date]').count)
    within(page.all('td[data-date]')[timeslot]) do
      if page.has_css?('span[class="fc-day-number"]')
        good = true
      end
    end
  end
  $booking_time = (Date.parse page.all('td[data-date]')[timeslot][:'data-date'])
  within(page.all('td[data-date]')[timeslot]) do
    find('span').click
  end
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

When(/^I pick the (?:staff member|resource) "([^']+)"$/) do |object|
  expect(page).to have_css('input[aria-label="Choose a person"]')
  find('input[aria-label="Choose a person"]').click
  expect(page).to have_css('a[class="ui-select-choices-row-inner"]')
  find('a[class="ui-select-choices-row-inner"]', text: object).click
  within('td[class="fc-resource-area fc-widget-content"]') do
    expect(page).to have_css('span[class="fc-cell-text"]', count: 2)
    expect(page).to have_css('span[class="fc-cell-text"]', text: object, match: :prefer_exact)
  end
end

When("I select the date {string} and time {string} on the calendar") do |date, time|
  select_date(date)
  collect_day_times
  collect_day_slots
  collect_day_bookings
  select_day_slot(time)
  click_day_slot
end

When("I select to make a booking for {string} on {string} at {string}") do |staff, day, time|
  select_date(day)
  find('span[class="slider  switch-left"]').click
  expect(page).to have_css('input[aria-label="Choose a person"]')
  find('input[aria-label="Choose a person"]').click
  expect(page).to have_css('a[class="ui-select-choices-row-inner"]')
  find('a[class="ui-select-choices-row-inner"]', text: staff).click
  within('td[class="fc-resource-area fc-widget-content"]') do
    expect(page).to have_css('span[class="fc-cell-text"]', count: 2)
    expect(page).to have_css('span[class="fc-cell-text"]', text: staff, match: :prefer_exact)
  end
  collect_day_times
  collect_day_slots
  select_day_slot(time)
  click_day_slot
end

When("I select a random timeslot") do
  collect_day_times
  collect_day_slots
  collect_day_bookings
  select_random_day_slot
  click_day_slot
end

When("I click on a timeslot on the week calendar") do
  $day = rand(7)
  collect_week_times($day)
  collect_week_slots
  collect_week_bookings($day)
  select_random_week_slot
  click_week_slot($day)
end

When("I select the day {string} at the time {string} on the week calendar") do |day, time|
  $day = day
  collect_week_times($day)
  collect_week_slots
  collect_week_bookings($day)
  select_week_slot(time)
  click_week_slot($day)
end

When("The booking widget is open and the correct date and time is displayed") do
  expect(page).to have_css('span[translate="ADMIN_BOOKING.QUICK_PICK.MAKE_BOOKING_TAB_HEADING"]')
  expect(page).to have_css('span[translate="ADMIN_BOOKING.QUICK_PICK.BLOCK_TIME_TAB_HEADING"]')
  expect(page).to have_css('select[name="service"]')
  expect(page).to have_css('button[translate="ADMIN_BOOKING.QUICK_PICK.NEXT_BTN"]')
  expect(page).to have_css('button[class="close"]')
  if $booking_time.strftime('%-I:%M %p') == '12:00 AM'
    expect(page).to have_css('h3', text: /#{$booking_time.strftime('%-d')}(?:st|nd|rd|th) #{$booking_time.strftime('%B %Y')}/)
  else
    expect(page).to have_css('h3', text: $booking_time.strftime('%A, %B %-d, %Y %-I:%M %p'))
  end
end

When("I select a service") do
  find('select[name="service"]').select 'Appointment'
  expect(page).to have_css('select[name="person"]')
end

When("I click Continue") do
  find('button[translate="ADMIN_BOOKING.QUICK_PICK.NEXT_BTN"]').click
end

When("The Select a time step is open") do
  expect(page).to have_css('h3', text: /#{$booking_time.strftime('%-d')}(?:st|nd|rd|th) #{$booking_time.strftime('%B %Y')}/)
  expect(page).to have_css('span[translate="ADMIN_BOOKING.CALENDAR.STEP_HEADING"]')
  expect(page).to have_css('select[id="person"]')
  expect(page).to have_css('select[id="resource"]')
  expect(page).to have_css('span[translate="ADMIN_BOOKING.CALENDAR.DAY_VIEW_BTN"]')
  expect(page).to have_css('button[class="btn btn-sm btn-default active"]', text: 'day')
  expect(page).to have_css('div[id="calendar-grid"]')
  expect(page).to have_css('button[translate="ADMIN_BOOKING.CALENDAR.SELECT_BTN"]')
  expect(page).to have_css('button', text: 'Back')
  expect(page).to have_css('button[class="close"]')
end

When("I select an available timeslot") do
  expect(page).to have_css('div[id="calendar-grid"]')
  while page.has_css?('div[class="accordion-toggle"]', text: '+') == false
    find('button[aria-lavel="Next Day"]').click
    $booking_time = $booking_time + 86400
    expect(page).to have_css('h1', text: /#{$booking_time.strftime('%-d')}(?:st|nd|rd|th) #{$booking_time.strftime('%B %Y')}/)
  end
  find('div[class="accordion-toggle"]', text: '+', match: :first).click
  within('ul[class="time-slots ng-scope"]', match: :first) do
    time = find('li', match: :first).text
    $booking_time = Time.parse(time + $booking_time.to_s)
    find('li', match: :first).click
  end
  expect(page).to have_css('span', text: $booking_time.strftime('%-I:%M %p'))
  expect(page).to have_css('h3', text: $booking_time.strftime('%A, %B %-d, %Y %-I:%M %p'))
  find('button[translate="ADMIN_BOOKING.CALENDAR.SELECT_BTN"]').click
end

When("The Select a customer step is open") do
  expect(page).to have_css('h3', text: $booking_time.strftime('%A, %B %-d, %Y %-I:%M %p'))
  expect(page).to have_css('h1[translate="ADMIN_BOOKING.CUSTOMER.STEP_HEADING"]')
  expect(page).to have_css('input[name="result"]')
  expect(page).to have_css('button[aria-label="Search for customer"]')
  expect(page).to have_css('input[id="first_name"]')
  expect(page).to have_css('input[id="last_name"]')
  expect(page).to have_css('input[id="email"]')
  expect(page).to have_css('div[class="selected-flag"]')
  expect(page).to have_css('input[id="mobile"]')
  expect(page).to have_css('button[translate="ADMIN_BOOKING.CUSTOMER.CREATE_BTN"]')
  expect(page).to have_css('button[translate="ADMIN_BOOKING.CUSTOMER.BACK_BTN"]')
  expect(page).to have_css('button[class="close"]')
end

When("I input new customer details") do
  $customer = new_user
  find('input[id="first_name"]').set $customer.firstname
  find('input[id="last_name"]').set $customer.surname
  find('input[id="email"]').set $customer.email
  find('input[id="mobile"]').set $customer.mobile
  find('div[class="selected-flag"]').click
  expect(page).to have_css('li[data-country-code="gb"]')
  find('li[data-country-code="gb"]', match: :first).click
  expect(page).to have_css('div[class="iti-flag gb"]')
  find('button[translate="ADMIN_BOOKING.CUSTOMER.CREATE_BTN"]').click
end

When("The booking questions step is open") do
  expect(page).to have_css('h3', text: $booking_time.strftime('%A, %B %-d, %Y %-I:%M %p'))
  expect(page).to have_css('h2[translate="PUBLIC_BOOKING.CHECK_ITEMS.BOOKING_QUESTIONS_HEADING"]')
  expect(page).to have_css('button[translate="ADMIN_BOOKING.CHECK_ITEMS.BOOK_BTN"]')
  expect(page).to have_css('button[translate="ADMIN_BOOKING.CHECK_ITEMS.BACK_BTN"]')
  expect(page).to have_css('button[class="close"]')
end

When("I click Book") do
  find('button[translate="ADMIN_BOOKING.CHECK_ITEMS.BOOK_BTN"]').click
end

When("The Summary step is open") do
  expect(page).to have_css('h3', text: $booking_time.strftime('%A, %B %-d, %Y %-I:%M %p'))
  expect(page).to have_css('h2[translate="PUBLIC_BOOKING.BASKET_SUMMARY.STEP_HEADING"]')
  expect(page).to have_css('li[id="bb-basket-summary-datetime"]', text: $booking_time.strftime('%a, %b %-d, %Y %-I:%M %p'))
  expect(page).to have_css('li[id="bb-basket-summary-duration"]', text: '15 minutes')
  expect(page).to have_css('li[id="bb-basket-summary-fullname"]', text: $customer.firstname + ' ' + $customer.surname)
  expect(page).to have_css('li[id="bb-basket-summary-email"]', text: $customer.email)
  expect(page).to have_css('button[translate="PUBLIC_BOOKING.BASKET_SUMMARY.CONFIRM_BTN"]')
  expect(page).to have_css('button[translate="PUBLIC_BOOKING.BASKET_SUMMARY.BACK_BTN"]')
  expect(page).to have_css('button[class="close"]')
end

When("I click Confirm") do
  find('button[translate="PUBLIC_BOOKING.BASKET_SUMMARY.CONFIRM_BTN"]').click
end

Then("The Confirmation step is open") do
  expect(page).to have_css('h3', text: $booking_time.strftime('%A, %B %-d, %Y %-I:%M %p'))
  expect(page).to have_css('h2[translate="ADMIN_BOOKING.CONFIRMATION.TITLE"]')
  expect(page).to have_css('li[id="bb-confirmation-summary-reference"]')
  expect(page).to have_css('li[id="bb-confirmation-summary-customer"]', text: $customer.firstname + ' ' + $customer.surname)
  expect(page).to have_css('li[id="bb-confirmation-summary-service"]', text: 'Appointment')
  expect(page).to have_css('li[id="bb-confirmation-summary-datetime"]', text: $booking_time.strftime('%a, %b %-d, %Y %-I:%M %p'))
  expect(page).to have_css('span[translate="ADMIN_BOOKING.CONFIRMATION.CLOSE_BTN"]')
  expect(page).to have_css('button[class="close"]')
  find('span[translate="ADMIN_BOOKING.CONFIRMATION.CLOSE_BTN"]').click
end

Then("I can see the booking on the calendar") do
  step 'I click on the "Agenda" calendar'
  step 'The Agenda calendar is open'
  select_date($booking_time) unless $booking_time == Date.today
  expect(page).to have_css('h1', text: $booking_time.strftime('%B %-d, %Y'))
  expect(page).to have_css('tr[class="fc-list-item status_booked"]')
  within('tr[class="fc-list-item status_booked"]', text: $customer.firstname + ' ' + $customer.surname) do
    expect(page).to have_content('Appointment')
    expect(page).to have_content($booking_time.strftime('%-I:%M%p').downcase)
  end
  $date = nil
  $time = nil
  $booking_time = nil
end

def select_date(date)
  if Date.parse(date.to_s) < Date.today && date.to_s.include?('day')
    $date = Date.parse(date.to_s) + 7
  else
    $date = Date.parse(date.to_s)
  end
  day = $date.strftime('%d')
  month = $date.strftime('%B')
  year = $date.strftime('%Y')
  unless $date == Date.today
    step 'I open the datepicker'
    step 'The datepicker is open'
    unless page.all('strong')[0].text == month + ' ' + year
      page.all('strong')[0].click
      expect(page).to have_css('button', text: month)
      unless page.all('strong')[0].text == year
        page.all('strong')[0].click
        expect(page).to have_css('button[class="btn btn-default"]', text: year)
        find('button[class="btn btn-default"]', text: year).click
        expect(page).to have_css('strong', text: year)
      end
      expect(page).to have_css('button', text: month)
      find('button', text: month).click
      expect(page).to have_css('strong', text: month)
    end
    within(find('div[class="uib-daypicker ng-scope"]')) do
      within(find('tbody')) do
        if page.all('button.btn.btn-default.btn-sm', text: day).count > 1
          if day.to_i > 20
            page.all('button.btn.btn-default.btn-sm', text: day)[1].click
          elsif day.to_i < 15
            page.all('button.btn.btn-default.btn-sm', text: day)[0].click
          end
        else
          find('button.btn.btn-default.btn-sm', text: day).click
        end
      end
    end
  end
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]', wait: 20)
  end
  expect(page).to have_css('h1', text: $date.strftime('%B %-d, %Y'))
end

def collect_day_times
  $calendar_length = page.evaluate_script("jQuery('td.fc-widget-content')[3].clientWidth")
  $calendar_half = $calendar_length / 2
  $slots = page.all('td[data-date]').count
  $slot_size = $calendar_length / $slots
  within('tr[class="fc-chrono"]') do
    $first_time = Time.parse(page.all('th').first['data-date'])
    $last_time = Time.parse(page.all('th').last['data-date']) + 60 * 60
  end
  $times = []
  while $first_time < $last_time
    $times.push($first_time)
    $first_time += (60 * 15)
  end
end

def collect_day_slots
  available_count = page.all('div[class="fc-bgevent"]').count
  x = 0
  $available_slots = []
  while x < available_count
    first_slot = page.all('div[class="fc-bgevent"]')[x]['style'][/left: (.*?)px/, 1].to_i
    last_slot = page.all('div[class="fc-bgevent"]')[x]['style'][/right: -(.*?)px/, 1].to_i
    first_slot /= $slot_size if first_slot > $slot_size
    last_slot /= $slot_size
    while first_slot < last_slot
      $available_slots.push(first_slot)
      first_slot += 1
    end
    x += 1
  end
end

def collect_day_bookings
  $unavailable_slots = []
  num = page.all('a.fc-timeline-event').count
  n = 0
  while n < num
    first_slot = page.all('a.fc-timeline-event')[n]['style'][/left: (.*?)px/, 1].to_i
    last_slot = page.all('a.fc-timeline-event')[n]['style'][/right: -(.*?)px/, 1].to_i
    first_slot /= $slot_size if first_slot > $slot_size
    last_slot /= $slot_size
    while first_slot < last_slot
      $unavailable_slots.push(first_slot)
      first_slot += 1
    end
    n += 1
  end
  $available_slots = $available_slots - $unavailable_slots
end

def select_day_slot(time)
  time = Time.parse(time)
  $date = Time.now if (defined? $date).nil?
  $booking_time = $date.strftime('%A, %B %-d, %Y') + time.strftime(' %-I:%M %p')
  $booking_time = Time.parse($booking_time)
  $time = $times.index($booking_time)
  $slot = $available_slots.index($time)
  abort 'The selected time is unavailable. Select a different time' if $slot == nil
  if $available_slots[$slot].zero?
    $selected_slot = ($calendar_half - 10) * -1
  else
    $selected_slot = ($slot_size * $available_slots[$slot])
    $selected_slot = if $selected_slot < $calendar_half
                       ($calendar_half * -1) + $selected_slot
                     else
                       $selected_slot - $calendar_half
                     end
  end
end

def select_random_day_slot
  $slot = rand($available_slots.count)
  if $available_slots[$slot].zero?
    $selected_slot = ($calendar_half - 10) * -1
  else
    $selected_slot = ($slot_size * $available_slots[$slot])
    $selected_slot = if $selected_slot < $calendar_half
                       ($calendar_half * -1) + $selected_slot
                     else
                       $selected_slot - $calendar_half
                     end
  end
  $booking_time = $times[$available_slots[$slot]]
end

def click_day_slot
  within('div[class="fc-rows"]') do
    find('td[class="fc-widget-content"]').hover
  end
  page.driver.browser.action.move_by($selected_slot + 5, 0).perform
  page.driver.browser.action.click.perform
end

def collect_week_times(day)
  if day.is_a? String
    day = Date.parse(day).strftime('%w').to_i
  end
  $calendar_length = page.evaluate_script("jQuery('div.fc-bgevent')[0].clientHeight").to_r
  $calendar_half = $calendar_length / 2
  $slots = page.all('tr[data-time]').count
  $slot_size = $calendar_length / $slots
  within(find('div[class="fc-scroller fc-time-grid-container"]')) do
    $first_time = Time.parse(Date.parse(Date::DAYNAMES[$day]).strftime('%d ') + Time.parse(page.all('tr[data-time').first['data-time']).strftime('%H:%M'))
    $last_time = Time.parse(Date.parse(Date::DAYNAMES[$day]).strftime('%d ')  + Time.parse(page.all('tr[data-time]').last['data-time']).strftime('%H:%M')) + 60 * 15
  end
  $times = []
  while $first_time < $last_time
    $times.push($first_time)
    $first_time += (60 * 15)
  end
end

def collect_week_slots
  $available_slots = []
  x = 0
  while x < $times.count
    $available_slots.push(x)
    x += 1
  end
end

def collect_week_bookings(day)
  if day.is_a? String
    day = Date.parse(day).strftime('%w').to_i
  end
  $unavailable_slots = []
  if page.all('div[class="fc-event-container"]')[day]
    within(page.all('div[class="fc-event-container"]')[day]) do
      num = page.all('a').count
      n = 0
      while n < num
        first_slot = page.all('a')[n]['style'][/top: (.*?)px/, 1].to_i
        last_slot = page.all('a')[n]['style'][/bottom: -(.*?)px/, 1].to_i
        first_slot /= $slot_size if first_slot > $slot_size
        last_slot /= $slot_size
        while first_slot < last_slot
          $unavailable_slots.push(first_slot)
          first_slot += 1
        end
        n += 1
      end
      $available_slots = $available_slots - $unavailable_slots
    end
  end
end

def select_week_slot(time)
  time = Time.parse(time)
  $date = Time.now if (defined? $date).nil?
  $booking_time = Date.parse($day).strftime('%-d ') + $date.strftime('%B %Y') + time.strftime(' %-I:%M %p')
  $booking_time = Time.parse($booking_time)
  $time = $times.index($booking_time)
  $slot = $available_slots.index($time)
  abort 'The selected time is unavailable. Select a different time' if $slot == nil
  if $available_slots[$slot].zero?
    $selected_slot = ($calendar_half - 10) * -1
  else
    $selected_slot = ($slot_size * $available_slots[$slot])
    $selected_slot = if $selected_slot < $calendar_half
                       ($calendar_half * -1) + $selected_slot
                     else
                       ($selected_slot + $slot_size) - $calendar_half
                     end
  end
end

def select_random_week_slot
  $slot = rand($available_slots.count)
  if $available_slots[$slot].zero?
    $selected_slot = ($calendar_half - 10) * -1
  else
    $selected_slot = ($slot_size * $available_slots[$slot])
    $selected_slot = if $selected_slot < $calendar_half
                       ($calendar_half * -1) + $selected_slot
                     else
                      ($selected_slot + $slot_size) - $calendar_half
                     end
  end
  $booking_time = Time.parse($times[$available_slots[$slot]].strftime('%H:%M') + Date.parse(Date::DAYNAMES[$day]).strftime(' %-d ') + $times[$available_slots[$slot]].strftime('%B %Y'))
end

def click_week_slot(day)
  if day.is_a? String
    day = Date.parse(day).strftime('%w').to_i
  end
  page.all('div[class="fc-bgevent"]')[day].hover
  page.driver.browser.action.move_by(0, $selected_slot).perform
  page.driver.browser.action.click.perform
end