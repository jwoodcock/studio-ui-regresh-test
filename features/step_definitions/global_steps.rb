Given("I visit the Studio URL") do
  visit('http://bespoke.bookingbug.com/studio/')
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]', wait: 10)
  end
end

Given("The login page is open") do
  expect(page).to have_css('div[class="login-logo-container"]')
  expect(page).to have_content('Login to view your account')
  expect(page).to have_content('Site')
  expect(page).to have_css('input[id="site"]')
  expect(page).to have_content('Username')
  expect(page).to have_css('input[id="username"]')
  expect(page).to have_content('Password')
  expect(page).to have_css('input[id="password"]')
  expect(page).to have_css('a', text: 'Forgot your password?')
  expect(page).to have_css('button', text: 'Login')
end

Given("I log in to the studio company") do
  find_field('site').set 'qauk-staging-2.bookingbug.com'
  find_field('username').set 'qa+bookings@bookingbug.com'
  find_field('password').set 'bn56tygh'
  click_button('Login')
  if page.has_css?('div[id="loading-bar-spinner"]')
    expect(page).to have_no_css('div[id="loading-bar-spinner"]')
  end
end

Given("The Studio is open") do
  expect(page).to have_css('a[class="logo"]')
  expect(page).to have_css('a[class="sidebar-toggle ng-scope off"]')
  expect(page).to have_css('a[class="dropdown-toggle"]', text: 'Company')
  expect(page).to have_css('a[class="dropdown-toggle"]', text: 'Hi,')
  expect(page).to have_css('aside[class="main-sidebar ng-scope"]')
end

Then("{string} is displayed") do |string|
  expect(page).to have_content(string)
end