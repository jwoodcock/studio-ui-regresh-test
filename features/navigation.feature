Feature: Navigate around the studio interface

  Background:
    Given I visit the Studio URL
    And The login page is open
    And I log in to the studio company

  Scenario: Open the Staff Calendar
    Given The Studio is open
    And I am not on the Staff Calendar
    When I open the Staff Calendar
    Then The "Staff" Calendar is open

  Scenario: Open the Resources Calendar
    Given The Studio is open
    When I open the Resources Calendar
    Then The "Resources" Calendar is open

  Scenario: Open the Customers page
    Given The Studio is open
    When I open the Customers page
    Then The Customers page is open

  Scenario: Open the Check In page
    Given The Studio is open
    When I open the Check In page
    Then The Check In page is open

  Scenario: Open the top right drop down menu
    Given The Studio is open
    When I open the top right drop down menu
    Then The top right drop down menu is open

  Scenario: Log out from the top right drop down menu
    Given The Studio is open
    And I open the top right drop down menu
    And The top right drop down menu is open
    When I click Logout
    Then I am logged out

  Scenario: Switch to Classic Bookingbug
    Given The Studio is open
    And I open the top right drop down menu
    And The top right drop down menu is open
    When I click Switch to Classic
    Then The classic rails app interface is open
