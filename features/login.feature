Feature: Logging in

  Background:
    Given I visit the Studio URL

  Scenario: Login with valid credentials
    Given The login page is open
    When I enter a valid url
    And I enter a valid email address for the bookings company
    And I enter a valid password for the bookings company
    And I click Login
    Then I am successfully logged in

  Scenario: Login with valid credentials for a different company
    Given The login page is open
    When I enter a valid url
    And I enter a valid email address for the config company
    And I enter a valid password for the config company
    And I click Login
    Then I am successfully logged in

  Scenario: Fail to login with no credentials
    Given The login page is open
    When I click Login
    Then I am not logged in
    And "This field is required" is displayed

  Scenario: Fail to login with an invalid url
    Given The login page is open
    When I enter an invalid string in the url field
    And I enter a valid email address for the bookings company
    And I enter a valid password for the bookings company
    And I click Login
    Then I am not logged in
    And "Sorry, either your email or password was incorrect" is displayed

  Scenario: Fail to login with an invalid email
    Given The login page is open
    When I enter an invalid string in the email field
    And I enter a valid url
    And I enter a valid password for the bookings company
    And I click Login
    Then I am not logged in
    And "Sorry, either your email or password was incorrect" is displayed

  Scenario: Fail to login with just a url
    Given The login page is open
    When I enter a valid url
    And I click Login
    Then I am not logged in
    And "This field is required" is displayed

  Scenario: Fail to login with just an email
    Given The login page is open
    When I enter a valid email address for the bookings company
    And I click Login
    Then I am not logged in
    And "This field is required" is displayed

  Scenario: Fail to login with just a password
    Given The login page is open
    When I enter a valid password for the bookings company
    And I click Login
    Then I am not logged in
    And "This field is required" is displayed

  Scenario: Fail to login with incorrect credentials
    Given The login page is open
    When I enter an incorrect url
    And I enter an incorrect email address
    And I enter an incorrect password
    And I click Login
    Then I am not logged in
    And "Sorry, either your email or password was incorrect" is displayed

  Scenario: Fail to login with an incorrect password
    Given The login page is open
    When I enter a valid url
    When I enter a valid email address for the bookings company
    And I enter an incorrect password
    And I click Login
    Then I am not logged in
    And "Sorry, either your email or password was incorrect" is displayed

  Scenario: Fail to login with an incorrect email address
    Given The login page is open
    When I enter an incorrect email address
    And I enter a valid url
    And I enter a valid password for the bookings company
    And I click Login
    Then I am not logged in
    And "Sorry, either your email or password was incorrect" is displayed

  Scenario: Fail to login with valid credentials from different users
    Given The login page is open
    When I enter a valid url
    And I enter a valid email address for the bookings company
    And I enter a valid password for the config company
    And I click Login
    Then I am not logged in
    And "Sorry, either your email or password was incorrect" is displayed
