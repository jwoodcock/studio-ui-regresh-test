Feature: Use the Staff calendar

  Background:
    Given I visit the Studio URL
    And The login page is open
    And I log in to the studio company
    And The Studio is open

  Scenario: Open the Staff Agenda Calendar
    Given The "Staff" Calendar is open
    When I click on the "Agenda" calendar
    Then The Agenda calendar is open

  Scenario: Open the Staff Week Calendar
    Given The "Staff" Calendar is open
    When I click on the "Week" calendar
    Then The Week calendar is open

  Scenario: Open the Staff Month Calendar
    Given The "Staff" Calendar is open
    When I click on the "Month" calendar
    Then The Month calendar is open

  Scenario: View tomorrow's staff calendar
    Given The "Staff" Calendar is open
    When I click the "right" arrow
    Then Tomorrow's calendar is open

  Scenario: View yesterdays's staff calendar
    Given The "Staff" Calendar is open
    When I click the "left" arrow
    Then Yesterday's calendar is open

  Scenario: Open a random date's calendar with the datepicker
    Given The "Staff" Calendar is open
    When I open the datepicker
    And The datepicker is open
    And I pick a random date
    Then The correct date's calendar will be open

  Scenario: Return to today's calendar after changing date
    Given The "Staff" Calendar is open
    And I click the "left" arrow
    And Yesterday's calendar is open
    When I click Today
    Then Today's calendar will be open again

  Scenario: Show an individual staff member's calendar
    Given The "Staff" Calendar is open
    When I click the show toggle
    And I select a staff member
    Then I can only see the individual staff member's calendar  

  Scenario: Make a booking with the day calendar
    Given The "Staff" Calendar is open
    And I click the show toggle
    And I pick the staff member "Staff 1"
    When I select a random timeslot
    And The booking widget is open and the correct date and time is displayed
    And I select a service
    And I click Continue
    And The Select a customer step is open
    And I input new customer details
    And The booking questions step is open
    And I click Book
    And The Summary step is open
    When I click Confirm
    Then The Confirmation step is open
    And I can see the booking on the calendar

  Scenario: Make a booking with the week calendar
    Given The "Staff" Calendar is open
    And I click on the "Week" calendar
    And The Week calendar is open
    When I click on a timeslot on the week calendar
    And The booking widget is open and the correct date and time is displayed
    And I select a service
    And I click Continue
    And The Select a customer step is open
    And I input new customer details
    And The booking questions step is open
    And I click Book
    And The Summary step is open
    When I click Confirm
    Then The Confirmation step is open
    And I can see the booking on the calendar

  Scenario: Make a booking with the month calendar
    Given The "Staff" Calendar is open
    And I click on the "Month" calendar
    And The Month calendar is open
    When I click on a date on the month calendar
    And The booking widget is open and the correct date and time is displayed
    And I select a service
    And I click Continue
    And The Select a time step is open
    And I select an available timeslot
    And The Select a customer step is open
    And I input new customer details
    And The booking questions step is open
    And I click Book
    And The Summary step is open
    When I click Confirm
    Then The Confirmation step is open
    And I can see the booking on the calendar

  Scenario: Select a specific timeslot in the past to book
    Given The "Staff" Calendar is open
    When I select to make a booking for "Staff 2" on "1st January 2013" at "4pm"
    Then The booking widget is open and the correct date and time is displayed

  Scenario: Select a specific timeslot in the future to book
    Given The "Staff" Calendar is open
    When I select to make a booking for "Staff 1" on "30th December 2020" at "9am"
    Then The booking widget is open and the correct date and time is displayed