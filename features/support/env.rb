require 'capybara/cucumber'
require 'selenium/webdriver'
require 'sauce_whisk'
require 'rake'
require 'parallel_tests'
require 'pry'
require 'faker'
require 'curb'
require_relative 'saucelabs_driver.rb'
require_relative 'chrome_driver.rb'

ENV['TEST_ENV'] ||= 'chrome'

if ENV['TEST_ENV'] == 'chrome'
  chrome()
end

if ENV['TEST_ENV'] == 'saucelabs'
  saucelabs()
end