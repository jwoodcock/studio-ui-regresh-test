def chrome()
  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new app, browser: :chrome,
    options: Selenium::WebDriver::Chrome::Options.new(args: %w[headless disable-gpu window-size=2000,1696])
  end

  Capybara.default_driver = :chrome
  Capybara.default_max_wait_time = 10

  After do |scenario|
    Capybara.current_session.driver.quit
  end
end
