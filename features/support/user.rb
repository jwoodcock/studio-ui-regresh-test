def new_user
  set_config
  username = putsbox_username
  user_attributes = {
    firstname: Faker::Name.first_name, surname: surname,
    mobile: Faker::PhoneNumber.phone_number.sub(/^[0:]*/,"").delete(' '), address: Faker::Address.street_name,
    postcode: Faker::Address.postcode, email: "#{username}@putsbox.com",
    inbox: "http://putsbox.com/#{username}/inspect/", password: Faker::Internet.password,
    town: Faker::Address.city
  }
  OpenStruct.new(user_attributes)
end

def surname
  surname = (Faker::Name.last_name)
  surname.slice! "'"
  surname
end

def set_config
  I18n.available_locales.push('en-GB')
  Faker::Config.locale = 'en-GB'
end

def putsbox_username
  putsbox_curl_html_payload = Curl.post('http://www.putsbox.com/buckets', { authenticity_token: auth_token_from_putsbox }).body_str
  get_putsbox_username_from(putsbox_curl_html_payload)
end

def auth_token_from_putsbox
  curl = Curl.get('http://www.putsbox.com')
  curl.body_str.split('csrf-token')[0].split('meta content=')[2].split('name')[0].split('\"')[1]
end

def get_putsbox_username_from(html_response)
  html_response.split('.com/')[1].split('/')[0]
end
