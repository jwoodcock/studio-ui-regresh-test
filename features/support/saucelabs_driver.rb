require 'selenium/webdriver'

module Saucelabs
  class << self
    attr_writer :desired_capabilities
    
    def username
      ENV['SAUCE_USERNAME']
    end

    def access_key
      ENV['SAUCE_ACCESS_KEY']
    end

    def authentication
      "#{username}:#{access_key}"
    end

    def sauce_server
      'ondemand.saucelabs.com'
    end

    def sauce_port
      80
    end

    def endpoint
      "http://#{authentication}@#{sauce_server}:#{sauce_port}/wd/hub"
    end

    def environment_capabilities
      browser = ENV['SAUCE_BROWSER']
      version = ENV['SAUCE_VERSION']
      platform = ENV['SAUCE_PLATFORM']

      if browser && version && platform
        return {
          :browserName => browser,
          :version => version,
          :platform => platform
        }
      end
      
      return nil
    end

    def default_capabilities
      {
        :browserName => "Chrome",
        :version => "59",
        :platform => "Windows 7"
      }
    end

    def desired_caps
      environment_capabilities || @desired_capabilities || default_capabilities
    end

    def webdriver_config
      {
        :browser => :remote,
        :url => endpoint,
        :desired_capabilities => desired_caps
      }
    end
  end
end

def saucelabs
  Capybara.register_driver :saucelabs_driver do |app|
    Capybara::Selenium::Driver.new app, Saucelabs.webdriver_config
  end
  Capybara.default_driver = :saucelabs_driver
  Capybara.default_max_wait_time = 10

  Around do |scenario, block|
    #variable for saucewhisk
    @driver = Capybara.current_session.driver
    @session_id = @driver.browser.session_id
    
    #create job names for saucelabs
    job = SauceWhisk::Jobs.fetch @session_id
    job.name = "#{scenario.feature.name} - #{scenario.name}"
    job.save
    
    block.call
    
    Capybara.current_session.driver.quit

    #report back to saucelabs with pass or fail
    if scenario.exception
      SauceWhisk::Jobs.fail_job @session_id
    else
      SauceWhisk::Jobs.pass_job @session_id
    end
  end
end