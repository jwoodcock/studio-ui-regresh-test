# BBQA-automation-2.0 alpha test

## Prerequisites

- RVM (https://rvm.io/rvm/install)
- A Saucelabs account (https://saucelabs.com/)

## Run the tests!

1. Clone the repo
2. Input the relevant Saucelabs details in bash.sh (found on your saucelabs account page)
3. Run ./bash.sh
4. Login to Saucelabs to view the results

## Don't want to run tests via Saucelabs?

1. Replace the final line of bash.sh with cucumber features
2. Run ./bash.sh
3. Tests are run in Headless Chrome instead