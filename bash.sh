#!/bin/bash --login
export SAUCE_USERNAME=JoelWoodcock
export SAUCE_ACCESS_KEY=3e5eae28-cc55-43df-823e-41e5548f45f8
LATESTVERSION=$(curl 'https://www.ruby-lang.org/en/downloads/'  | grep -A2 'Stable releases' | grep -Po '(?<=(Ruby )).*(?=</a>)')
LIST=$(rvm list)
if [[ $LIST == *"$LATESTVERSION"* ]]; then 
  rvm use $LATESTVERSION
  bundle install
else
  rvm install $LATESTVERSION
  rvm use $LATESTVERSION
  gem install bundler
  bundle install
fi
cucumber